<?php require_once"./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity s03</title>
</head>
<body>

	<!-- Activity -->

	<h1>Person</h1>
	<p><?= $person -> printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $developer -> printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $engineer -> printName(); ?></p>

	<!-- Supplementary Activity -->
	<h1>Voltes V</h1>
	<p><?php var_dump($voltesMember); ?></p>   
	<p><?php var_dump($voltesMember1); ?></p>
	<p><?php var_dump($voltesMember2); ?></p>
	<p><?php var_dump($voltesMember3); ?></p>
	<p><?php var_dump($voltesMember4); ?></p>


	<p><?= $voltesMember -> printName(); ?></p>
	<p><?= $voltesMember1 -> printName(); ?></p>
	<p><?= $voltesMember2 -> printName(); ?></p>
	<p><?= $voltesMember3 -> printName(); ?></p>
	<p><?= $voltesMember4 -> printName(); ?></p>

</body>
</html>
