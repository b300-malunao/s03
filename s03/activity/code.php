<?php

class Person {

	public $firstName;
	
	public $lastName;

	public function __construct($firstName, $lastName){
		
		$this->firstName = $firstName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->lastName";
	}

};

$person = new Person("Senku","Ishigami");


Class Developer extends Person{

	public $middleName;

	public function __construct($firstName, $middleName, $lastName){
		parent::__construct($firstName, $lastName);
		$this->middleName = $middleName;
	}

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
	}


}

$developer = new Developer("John", "Finch", "Smith");

Class Engineer extends Person{

	public $middleName;

	public function __construct($firstName, $middleName, $lastName){
		parent::__construct($firstName, $lastName);
		$this->middleName = $middleName;
	}

	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName";
	}


}

$engineer = new Engineer("Harold", "Myers", "Reese");

// Supplementary Activity
class Character {

	public $name;

	public function __construct($name) {
		return "Hi, I am $this->name";
	}
}

class VoltesMember extends Character {

	public $vehicle;

	public function __construct($name, $vehicle) {

		parent::__construct($name);
		$this -> vehicle = $vehicle;
	}

	public function printName() {
		return "Hi, I am $this->name! I pilot the $this->vehicle!";
	}
}


$voltesMember = new VoltesMember("Steve", "Volt Cruiser");
$voltesMember1 = new VoltesMember("Big Bert", "Volt Panzer");
$voltesMember2 = new VoltesMember("Little John", "Volt Frigate");
$voltesMember3 = new VoltesMember("Jamie", "Volt Lander");
$voltesMember4 = new VoltesMember("Mark", "Volt Bomber");
